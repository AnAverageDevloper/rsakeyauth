package utils

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"errors"
)

func ParseRSAPublicKeyFromPEM(pemData []byte) (*rsa.PublicKey, error) {
	block, _ := pem.Decode(pemData)
	if block == nil {
		return nil, errors.New("failed to parse PEM block containing the public key")
	}

	pubKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	pubKey, ok := pubKeyInterface.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("failed to assert public key type")
	}

	return pubKey, nil
}

func ConvertRSAPublicKeyToPEM(pubKey *rsa.PublicKey) ([]byte, error) {
	pubKeyBytes, err := x509.MarshalPKIXPublicKey(pubKey)
	if err != nil {
		return nil, err
	}

	pemData := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubKeyBytes,
	})

	return pemData, nil
}

// EncryptRSA encrypts the data using RSA encryption with the given public key.
func EncryptRSA(publicKey rsa.PublicKey, data []byte) ([]byte, error) {
	// Convert public key to the RSA public key format
	rsaPublicKey := &rsa.PublicKey{
		N: publicKey.N,
		E: publicKey.E,
	}

	// Encrypt the data using RSA-OAEP encryption
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		rsaPublicKey,
		data,
		nil,
	)
	if err != nil {
		return nil, err
	}

	return ciphertext, nil
}
