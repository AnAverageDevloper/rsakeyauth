package model

import "crypto/rsa"

type User struct {
	Username  string
	PublicKey rsa.PublicKey
}
