# PASSWORDLESS AUTH

**Simple passwordless authentication system.**

## Here's a simple explanation of how this authentication system works:

1. User Registration:
   - When a user wants to register, they provide a unique username and an RSA public key.
   - The system creates a new user object with the username and public key.
   - The user object is then stored in the database for future reference.

2. User Login:
   - When a user wants to log in, they provide their username and an encrypted key.
   - The system retrieves the user's information from the database based on the provided username.
   - A secret code is generated, which will be used for authentication.
   - The secret code is encrypted using the user's public key and sent back to the user.
   - The user's application decrypts the received encrypted code using their private key.
   - The decrypted code is sent back to the server for verification.
   - The server compares the decrypted code with the original secret code to validate the user's identity.
   - If the codes match, the login process is considered successful.

The system utilizes RSA encryption for secure communication between the server and the user. RSA encryption ensures that the secret code can only be decrypted by the user possessing the corresponding private key.

By storing the user's public key during registration and encrypting the secret code with it during login, the system ensures that only the user with the corresponding private key can decrypt and verify the code. This helps to authenticate the user and protect their sensitive information.

This also has the benefit that even if, for some reason, the server database gets leaked, no compromise is made.