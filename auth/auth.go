package auth

import (
	"crypto/rand"
	"crypto/rsa"
	"errors"

	"github.com/AnAverageBeing/rsakeyauth/db"
	"github.com/AnAverageBeing/rsakeyauth/model"
	"github.com/AnAverageBeing/rsakeyauth/utils"
)

// AuthEngine is responsible for user authentication operations.
type AuthEngine struct {
	db            db.DB
	screatKeySize uint
}

func NewAuthEngine(DB db.DB, ScreatKeySize uint) *AuthEngine {
	return &AuthEngine{
		db:            DB,
		screatKeySize: ScreatKeySize,
	}
}

// RegisterUser registers a new user with a unique username and public key.
func (ae *AuthEngine) RegisterUser(username string, publicKey *rsa.PublicKey) error {
	user := &model.User{
		Username:  username,
		PublicKey: *publicKey,
	}

	err := ae.db.CreateUser(user)
	if err != nil {
		return errors.New("failed to register user: " + err.Error())
	}

	return nil
}

// GetCode returns the screat code and encrypted screat code
func (ae *AuthEngine) GetCode(username string) (code []byte, encryptedCode []byte, err error) {
	user, err := ae.db.GetUserByUsername(username)
	if err != nil {
		err = errors.New("failed to get user: " + err.Error())
		return
	}

	// Generate a secret code
	code = make([]byte, ae.screatKeySize)
	_, err = rand.Read(code)
	if err != nil {
		err = errors.New("failed to generate secret code: " + err.Error())
		return
	}

	// Encrypt the secret code with the user's public key
	encryptedCode, err = utils.EncryptRSA(user.PublicKey, code)
	if err != nil {
		err = errors.New("failed to encrypt secret code: " + err.Error())
		return
	}

	return
}
