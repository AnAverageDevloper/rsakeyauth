package db

import "github.com/AnAverageBeing/rsakeyauth/model"

// DB represents the interface for interacting with a database.
type DB interface {
	// CreateUser creates a new user in the database.
	CreateUser(user *model.User) error

	// GetUserByUsername retrieves a user from the database based on their username.
	GetUserByUsername(username string) (*model.User, error)

	// UpdateUser updates an existing user in the database.
	UpdateUser(user *model.User) error

	// DeleteUser deletes a user from the database.
	DeleteUser(username string) error
}
